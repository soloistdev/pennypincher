<?php
require '../vendor/autoload.php';
header('Access-Control-Allow-Origin: *'); 
use App\Core\Response;
use App\Controllers\TravelRoutesController;

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $route) {
	require '../api/routes.php';
});

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {  
    case FastRoute\Dispatcher::NOT_FOUND:
        Response::json(404,'ops, falha no engano');
        break; 
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        Response::json(405,'pode não!',$allowedMethods);
        break;

    case FastRoute\Dispatcher::FOUND:
        $handler = explode('::', $routeInfo[1]);
        $obj     = new TravelRoutesController;
        $method  = $handler[1];
        $vars    = $routeInfo[2];
        call_user_func_array(array($obj,$method),$vars);
        break;
}