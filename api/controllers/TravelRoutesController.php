<?php 

namespace App\Controllers;  
    
use App\Models\TravelRoute;
use App\Core\Dijkstra;

class TravelRoutesController
{

        /**
     * Criar uma rota;
     */
    public function create()
    {
        // Takes raw data from the request
        $json = file_get_contents('php://input');

        // Converts it into a PHP object
        $data = json_decode($json,true);
        TravelRoute::create(new TravelRoute($data["from"],$data["to"],$data["price"]));
   
        echo json_encode(TravelRoute::list());
    }

    /**
    * Criar uma rota;
    */
    public function places()
    {
        $routes = TravelRoute::places();
        echo json_encode($routes);
    }

    
     /**
     * Remover uma rota;
     */
    public function delete($pos)
    {		
        TravelRoute::delete($pos);
        echo json_encode(TravelRoute::list());
    }


        /**
     * Listar as rotas existentes;
     */
    public function list()
    {
        echo json_encode(TravelRoute::list());
    }

    /**
     * Funcão para cotar viagem menos custosa do ponto A ao ponto B (Dijkstra);
     * Entrada: Nome do ponto A e nome do ponto B;
     * Retorno: Lista de conexões e valores de menor custo para chegar de A a B;
     */
    public function quote($from, $to)
    {	
       $graph       = TravelRoute::graph();
       $finder      = new Dijkstra($graph); 
       $route       = $finder->findPath($from, $to);
       $cost        = $finder->cost;
       $connections = array();       
       
       if(count($route) > 0){
            $total = count($route);
            for ($i=0; $i <= $total-2; $i++) { 
                $connections[$i] = ["from"=>$route[$i],"to"=>$route[$i+1],"price"=>$finder->prices[$i]];
            }	
        }
        
        echo json_encode(['route'=>$route,'price'=>$cost,'connections'=>$connections]);

       

       
    }     
}
    

?>