<?php

	namespace App\Database;

	class Database 
	{
		//Váriaveis que especificam o banco de dados
		const FILEPATH  = "../api/database/routes.csv";
		const TEMPPATH  = "../api/database/temp.csv";
		const SEPARATOR = ",";
		const ROWSIZE   = 20;
		private static $_rows  = array();


        public static function list()
		{
			$row = 0;
			if (($handle = fopen(self::FILEPATH, "r")) !== FALSE) {
				self::$_rows = array();

				while (($data = fgetcsv($handle, self::ROWSIZE, self::SEPARATOR)) !== FALSE) {
					$num = count($data);					
					for ($cont=0; $cont < $num; $cont++) {
						self::$_rows[$row][$cont] = $data[$cont] ;
					}
					$row++;
				}
				fclose($handle);
			} 

			return self::$_rows;
		}

		public static function add($line)
		{
			if (($handle = fopen(self::FILEPATH, "a")) !== FALSE) {
				fputcsv($handle, $line);
				fclose($handle);
			} 
		}

		public static function remove($pos)
		{
			$handle     = fopen(self::FILEPATH,'r');
			$temp       = fopen(self::TEMPPATH,'w');

			$row = 0;
			while (($data = fgetcsv($handle, self::ROWSIZE)) !== FALSE){
				if($row != $pos)
					fputcsv($temp,$data);		
				$row++;			
			}

			fclose($handle);
			fclose($temp);
			rename(self::TEMPPATH,self::FILEPATH);
		}

	}
?>