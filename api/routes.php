<?php

$route->addRoute('GET', '/api/quote/{from}/{to}', 'TravelRoutesController::quote');
$route->addRoute('GET', '/api/places/', 'TravelRoutesController::places');
$route->addRoute('GET', '/api/list/', 'TravelRoutesController::list');
$route->addRoute('DELETE', '/api/delete/{pos}', 'TravelRoutesController::delete');
$route->addRoute('POST', '/api/route/', 'TravelRoutesController::create');

?>