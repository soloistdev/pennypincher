<?php

//Retirado de: https://rosettacode.org/wiki/Dijkstra%27s_algorithm

namespace App\Core;

class Dijkstra
{
    public $graph;
    public $cost;
    public $prices;

    public function __construct($graph)
    {
        $this->graph = $graph;
        $this->prices = array();
    }   

    public function findPath($source, $target) {
        $vertices = array();
        $neighbours = array();
        foreach ($this->graph as $edge) {
            array_push($vertices, $edge[0], $edge[1]);
            $neighbours[$edge[0]][] = array("end" => $edge[1], "cost" => $edge[2]);
        }
        $vertices = array_unique($vertices);
     
        foreach ($vertices as $vertex) {
            $dist[$vertex] = INF;
            $previous[$vertex] = NULL;
        }
     
        $dist[$source] = 0;
        $prev = 0;
        $Q = $vertices;
        $cont = 0;
        while (count($Q) > 0 && $cont <= count($vertices)) {

            $min = INF;
            foreach ($Q as $vertex){
                if ($dist[$vertex] < $min) {
                    $min = $dist[$vertex];
                    $u = $vertex;
                }
            }

            if($dist[$u]>0)
                $this->prices[] =  $dist[$u] - $prev; 
            
    
            $Q = array_diff($Q, array($u));
            if ($dist[$u] == INF or $u == $target) {
                break;
            }
    
            if (isset($neighbours[$u])) {
                foreach ($neighbours[$u] as $arr) {
                    $alt = $dist[$u] + $arr["cost"];                                   
                    if ($alt < $dist[$arr["end"]]) {                          
                        $dist[$arr["end"]] = $alt;
                        $previous[$arr["end"]] = $u;
                    }
                }
            } 
            $cont++;
            $prev =  $dist[$u];            
        }     

        $this->cost = $dist[$u];  

        $path = array();
        $u = $target;
        while (isset($previous[$u])) {
            array_unshift($path, $u);
            $u = $previous[$u];
        }
        array_unshift($path, $u);

        if(count($path) == 1)
            $this->cost = 0;  

        return $path;
    }   
}