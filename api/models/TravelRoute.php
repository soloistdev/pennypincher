<?php

	namespace App\Models;

	use App\Database\Database;

	class TravelRoute 
	{
		protected  $from;
		protected  $to;
		protected  $price;


		public function __construct($from,$to ,$price)
		{
			$this->from = $from;
			$this->to   = $to ;
			$this->price = $price;
		}

		
		public static function create(TravelRoute $route) {
			$line = [$route->from, $route->to, $route->price];
			Database::add($line);
		}
					
		public static function delete($pos) {
			Database::remove($pos);			
		}

		public static function places() {
			$rows = Database::list();
			$routes = array();
			foreach ($rows as $row){
				$routes[] = $row[0];
				$routes[] = $row[1];
			}			

			return array_values(array_unique($routes));
		}
			
		public static function list() {
			$rows = Database::list();
			$routes = array();
			foreach ($rows as $row) 
				$routes[] = array('from'=>$row[0],'to'=>$row[1],'price'=>$row[2]);			

			return $routes;
		}

		public static function graph() {
			$graph = array();
			$rows = Database::list();
			
			foreach ($rows as $row) 
				$graph[] = [$row[0],$row[1],$row[2]];			

			return $graph;
		}

	}
?>