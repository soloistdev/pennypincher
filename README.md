## Instalando dependências:
1. Executar o comando 'composer install' no prompt, instalando as dependencias/
2. Direcionar o Servidor para a pasta raiz public/

---

![Tela da Aplicação](tela.png)

---

## Estrutura das Pastas

- **Api:** Código do Backend  
- **Frontcode:** Código do Frontend  
- **Public:** Frontend Estático  
- **Tests:** Testes Unitários  
- **Resources:** Fontes Utilizadas 

---
