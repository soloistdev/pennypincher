import { AlertService } from './../../services/alert-service';
import {AppComponent} from '../../app.component';
import { Component, OnInit, Injectable } from '@angular/core';
import pageSettings from '../../config/page-settings';
import {TravelRoute} from '../../models/travelroute';
import {TravelRoutesService} from '../../services/travel-routes-service';

@Component({
  selector: 'home',
  templateUrl: './home.html',
  styleUrls: ['./home.css']
})

export class HomePage implements  OnInit {
  pageSettings = pageSettings;

  public routes:      TravelRoute[];
  public newRoute:    TravelRoute;
  public departure:   string;
  public arrival:     string;
  public connections: TravelRoute[];
  public totalCost:   number;
  public places:      string[];
  public processing:  boolean;

  constructor(
    private app:AppComponent, 
    private alertService: AlertService,
    private travelRoutesService : TravelRoutesService ) {
    this.pageSettings.pageWithoutSidebar = true; 
    this.pageSettings.pageWithFooter = true;  
  }

  ngOnInit() {
    this.places      = ['GRU','CLV'];
    this.connections = [{from:"GRU",to:"BRA",price:14},{from:"BRA",to:"CLV",price:12}];
    this.routes      = [{from:"GRU",to:"BRA",price:14},{from:"BRA",to:"CLV",price:12}];
    this.arrival   = 'CLV';
    this.departure = 'GRU';
    this.processing = false;
    this.totalCost = 26;
    this.newRoute = new TravelRoute("","",0);

    this.getPlaces();
    this.getRoutes();

  }


  getPlaces()
  {
    this.travelRoutesService
    .places()
    .subscribe(places => {
      this.places = places; 

      if(places.length > 1)
      {
        this.departure = places[0];
        this.arrival   = places[1];
      }
    },
    this.cbHandlerError.bind(this));
  }

  getRoutes()
  {
    this.travelRoutesService
    .list()
    .subscribe(routes => {
      this.routes = routes; 
    },
    this.cbHandlerError.bind(this));
  }


  create()
  {
    this.travelRoutesService
    .create(this.newRoute)
    .subscribe(routes => {
      this.routes = routes;
      this.newRoute = new TravelRoute("","",0);
    },
    this.cbHandlerError.bind(this));
  }

  delete(pos:number)
  {
    this.travelRoutesService
    .delete(pos)
    .subscribe(routes => {
      this.routes = routes; 
    },
    this.cbHandlerError.bind(this));
  }

  quote()
  {
      if(this.departure == "" || this.arrival == ""){
      this.alertService.error('ERRO', 'Selecione ambos, partida e destino!', 'OK');
    return;}

      this.processing = true;

      this.travelRoutesService.quote(this.departure,this.arrival) .subscribe(data => {
        this.connections = data.connections;
        this.totalCost   = data.price;
        this.processing = false;
      },
      this.cbHandlerError.bind(this));
  }


  cbHandlerError(error: any) {
    console.log('ERROR', error);
    this.alertService.error('ERRO', 'Opa, falha no engano.', 'OK');
    this.processing = false;    
  }

}
