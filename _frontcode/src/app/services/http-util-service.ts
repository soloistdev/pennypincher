import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpResponse, HttpHeaders} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class HttpUtilService {
  constructor(private router: Router) { }

  private API_URL = "http://localhost/api/";

  url(path: string) {
      return this.API_URL + path;
  }

  headers() {
    let headersParams = new HttpHeaders({ Accept: "application/json" });
    const options = { headers: headersParams };
    return options;
  }

  extrairDados(response: HttpResponse<any>) {
    let data = response;
    return data || {};
  }

  processarErros(erro: any) {
    return Observable.throw(erro);
  }
}
