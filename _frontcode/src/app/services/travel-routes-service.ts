import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpUtilService} from './http-util-service';
import 'rxjs/add/operator/map';
import { TravelRoute } from '../models/travelroute';
import { Quote } from '../models/quote';

@Injectable()
export class TravelRoutesService {
    constructor(private http: HttpClient, private httpUtil: HttpUtilService) { }


    //[GET] Listar todas as rotas
    list() {
        return this.http.get<TravelRoute[]>(this.httpUtil.url('list/'));
    }

    //[GET] Listar todos os locais
    places()
    {
        return this.http.get<string[]>(this.httpUtil.url('places/'));
    }

    //[GET] Cotar valor para viagem
    quote(departure: string, arrival:string) {
        return this.http.get<Quote>(this.httpUtil.url('quote/') + departure + '/' + arrival);
    }

    //[POST] Criar uma rota
    create(travelroute: TravelRoute) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type':  'application/json',
            })
        };
        return this.http.post<TravelRoute[]>(this.httpUtil.url('route/'), travelroute, httpOptions);
    }

    //[DELETE] Remover uma rota
    delete(pos: number) {
        return this.http.delete<TravelRoute[]>(this.httpUtil.url('delete/') + pos);
    }

}
