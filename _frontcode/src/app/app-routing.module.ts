import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomePage }  from './pages/home/home';

const routes: Routes = [
  { path: '', component: HomePage, data: { title: 'PennyPincher Travels'},pathMatch: 'full'} 
];

@NgModule({
  imports: [ CommonModule, RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  declarations: []
})


export class AppRoutingModule { }
