
// Core Module
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule }               from '@angular/platform-browser/animations';
import { BrowserModule, Title }                  from '@angular/platform-browser';
import { HttpClientModule, HttpClient }          from '@angular/common/http';
import { NgModule }                              from '@angular/core';
import { FormsModule, ReactiveFormsModule }      from '@angular/forms';
import { AppRoutingModule }                      from './app-routing.module';
import { NgbModule }                             from '@ng-bootstrap/ng-bootstrap';
import * as global                               from './config/globals';

// Main Component
import { AppComponent }                    from './app.component';
import { HeaderComponent }                 from './components/header/header.component';
import { FooterComponent }                 from './components/footer/footer.component';

// Component Module
import { AgmCoreModule }                   from '@agm/core';
import { LoadingBarRouterModule }          from '@ngx-loading-bar/router';
import { SweetAlert2Module }               from '@sweetalert2/ngx-sweetalert2'
import { HomePage }            from './pages/home/home';
import { AlertService }        from './services/alert-service';
import { TravelRoutesService } from './services/travel-routes-service';
import { HttpUtilService }     from './services/http-util-service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomePage
  ],
  imports: [
    AppRoutingModule,
    AgmCoreModule.forRoot({ apiKey: 'AIzaSyC5gJ5x8Yw7qP_DqvNq3IdZi2WUSiDjskk' }),
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,    
    LoadingBarRouterModule,
    SweetAlert2Module.forRoot(),
    NgbModule,
    ReactiveFormsModule,
  ],
  providers: [ 
    Title, 
    AlertService,
    HttpUtilService,
    TravelRoutesService,],
  bootstrap: [ AppComponent ]
})

export class AppModule {
  constructor(private router: Router, private titleService: Title, private route: ActivatedRoute) {
    router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        var title = 'PennyPincher Travels| ' + this.route.snapshot.firstChild.data['title'];
        this.titleService.setTitle(title);
      }
    });
  }
}
