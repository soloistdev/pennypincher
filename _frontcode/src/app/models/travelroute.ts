export class TravelRoute {
    constructor(
        public from?: string,
        public to?: string,
        public price?: number
    ) { }
}
