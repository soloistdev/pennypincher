import { TravelRoute } from './travelroute';
export class Quote {
    constructor(
        public route?:       string[],
        public price?:       number,
        public connections?: TravelRoute[]
    ) { }
}
